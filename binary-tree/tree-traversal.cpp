#include <iostream>
#include <memory>

template<typename T>
class BinaryTreeNode {
public:
	T data;
	std::unique_ptr<BinaryTreeNode<T>> left, right;

	BinaryTreeNode(T cdata, std::unique_ptr<BinaryTreeNode> &cleft, std::unique_ptr<BinaryTreeNode> &cright):
		data{cdata}, left {std::move(cleft)}, right {std::move(cright)} 
	{}
};

void treeTraversal(const std::unique_ptr<BinaryTreeNode<int>> &root) {
	if (root) {
		std::cout << "Preorder: " << root->data;
		treeTraversal(root->left);
		std::cout << "Inorder: " << root->data;
		treeTraversal(root->right);
		std::cout << "Post order: " << root->data;
	}
}

int main(void) {
	// Create a tree
	std::unique_ptr<BinaryTreeNode<int>> left = std::make_unique<BinaryTreeNode<int>>(2, nullptr, nullptr);
	std::unique_ptr<BinaryTreeNode<int>> right = std::make_unique<BinaryTreeNode<int>>(3, nullptr, nullptr);	
	std::unique_ptr<BinaryTreeNode<int>> root = std::make_unique<BinaryTreeNode<int>>(1, &left, &right); 

	treeTraversal(root);
}
