# Problem Solving Practice
---

The purpose of this repo is to keep my problem solving skills sharp and 
get better at C++. 

Some of the tasks that I will take extra care of are:
- COMMIT EVERY DAY!
- creating libraries for the common data structures
- push every problem I solve
- write code in C++17 
- Once I become comfortable with C++17, try to port that to C++20

My approach will be to solve a few problems from EPI first, and 
then move on to leetcode and solve as many problems related to a 
particular topic, till I feel comfortable about it!

The aim is to atleast have the data structures and algorithms always
fresh in my mind!

Days not worked on this:
- April 20, 2021
- April 21, 2021 
